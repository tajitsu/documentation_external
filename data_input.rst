
Types of data
==============

Depending on your type of business you will have access to different
types of data that you have collected from your customers. For a
gaming company the data usually contains:

 * Customer information.
 * Transaction/session information, (i.e data generated when a
   customer plays one of your games).
 * Game information, (i.e data about what games are available, their
   names and denomination etc).
 * Bonus data from when a players receives a bonus of some sort.
 * Action/campaign data from when players are sent a campaign offer.
   
For a retail company the data usually contains:

 * Customer information.
 * Purchasing data.
 * Action/campaign data.

Depending on which of our services you will be using we will need
different type of data, but as a rule we would at least need customer
information and transaction/purchase data. For a more complete listing
of what data we handle for different businesses, see **here**

In general we can divide the data into these categories:

 * Data describing a customer, a product/game or an action. At a
   minimum we need to know which customers you have and some basic
   information about them. If you want to
   use our product recommendation service we also need to know which
   products are available and if you want to use our action
   recommendation service we need to know which actions are available.
 * Data describing when a customer "does something". For example when
   a customer plays one of your games or buys one of your products.
 * Data describing when you give your customers something or
   communicates with them. For example promotional emails and bonuses.

For a complete listing of which event types we support, see **here**
   

Sending data to Tajitsu
=======================

There are two basic ways of delivering data to us:

 * In batches where you upload CSV files containing a day/week/months
   worth of events to our FTP.
 * In realtime where you send events to our API as they happen.

You are free to choose which way that best suits your needs. An
important factor to think about when choosing is that we of course cannot give
insights, recommendations or actions on data that we do not have. So
if you want to be able to get recommendations based on customer
behaviour that happens "now", you need to send us data in realtime (or
in frequent batches).

The data you send to us (whether in batch or realtime) can either be
converted to our standard format by you before sending it to us, or we
can create a parser that translates your data into our format. Sending
data to us on our standard format allows for faster startup time and
is cheaper for you (we will charge for implementing the parser from your
format).
Our standard format is documented in detail **here**


Batch uploading
---------------

If you choose to deliver data in batches you create CSV files
containing the data and upload these to our FTP. If you use our
standard format detailed instructions about how these files should be
named and formated can be found **here**.
If we create a parser for your data the format is more flexible and
will be decided together with you.

The FTP server
^^^^^^^^^^^^^^

We use Secure FTP (SFTP) which means the communication between your
server and our will be encrypted. (You might need to explicitly choose
to use the SFTP propocol in your FTP client since SFTP uses a
different port than regular FTP).

We will create an FTP account for you and send you instructions on how
to log on to the server.
To upload a new batch of files you do the following steps:

  * Log in to the server using your account information.
  * Go to subdirectory "data".
  * Create a new directory named after todays date on format: YYYYMMDD.
  * Upload files into directory.

The files that you upload should end with .csv if they are ordinary
CSV files. You can also compress the CSV files into one or more files,
the file should then end with .zip, .tar.gz of .gz depending on type
of compression used.


Realtime uploading
------------------

If you choose to send event data in realtime you use our tracking API
endpoint. The endpoint takes a JSON object with event type and event
payload. Detailed information about how to send events to our API can
be found `here <http://api-docs.tajitsu.com/#track_event>`_


